﻿using PMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using User_Management_System;
using WebPrac.Security;

namespace WebPrac.Controllers
{
    public class UserController : Controller
    {
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        } 
        
        [HttpGet]
        public ActionResult Signup()
        {
            ViewBag.setting = "Enable";
            int userId;
            userId = Convert.ToInt32(Request.QueryString["userId"]);
            if (userId > 0)
            {
                var obj = PMS.BAL.UserBO.GetUserById(userId);
                ViewBag.Name = obj.Name;
                ViewBag.Login = obj.Login;
                ViewBag.Email = obj.Email;
                ViewBag.PictureName = obj.PictureName;
                ViewBag.setting = "disabled";
            }
            return View();
        }

        [HttpPost]
        public ActionResult Signup(UserDTO obj)
        {
            ViewBag.setting = "Enable";    
            ViewBag.Name = obj.Name;
            ViewBag.Login = obj.Login;
            ViewBag.Email = obj.Email;
            ViewBag.PictureName = obj.PictureName;
            return View();
        }

        [HttpPost]
        public ActionResult Login(String login, String password)
        {
            var obj = PMS.BAL.UserBO.ValidateUser(login, password);
            if (obj != null)
            {
                Session["user"] = obj;
                if (obj.IsAdmin == true)
                    return Redirect("~/Home/Admin");
                else
                    return Redirect("~/Home/NormalUser");
            }

            ViewBag.MSG = "Invalid Login/Password";
            ViewBag.Login = login;

            return View();
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(UserDTO dto)
        {
            if (dto.Password == null)
            {
                ViewBag.MSG = "Provide Password!";
                return View();
            }
            PMS.BAL.UserBO.UpdatePassword(dto);
            TempData["Msg"] = "Password is Updated!";
            return Redirect("~/home/admin");
        }

        [HttpPost]
        public ActionResult Save(UserDTO dto)
        {
            var uniqueName = "";
            if (Request.Files["Image"] != null)
            {
                var file = Request.Files["Image"];
                if (file.FileName != "")
                {
                    var ext = System.IO.Path.GetExtension(file.FileName);

                    //Generate a unique name using Guid
                    uniqueName = Guid.NewGuid().ToString() + ext;

                    //Get physical path of our folder where we want to save images
                    var rootPath = Server.MapPath("~/UploadedFiles");

                    var fileSavePath = System.IO.Path.Combine(rootPath, uniqueName);

                    // Save the uploaded file to "UploadedFiles" folder
                    file.SaveAs(fileSavePath);

                    dto.PictureName = uniqueName;
                }
            }

            String setting = Request["setting"];
            if(dto.Name==null || dto.Login==null || (dto.Password==null && setting != "disabled") || dto.Email=="" || dto.PictureName==null)
            {
                ViewBag.Name = dto.Name;
                ViewBag.Login = dto.Login;
                ViewBag.PictureName = dto.PictureName;
                ViewBag.Email = dto.Email;
                ViewBag.MSG = "Provide All Information with Picture!";
                return View("Signup");
            }


            PMS.BAL.UserBO.Save(dto);
            if(dto.UserID>0)
            {
                TempData["Msg"] = "User is Updated!";
                return Redirect("~/home/admin");
            }
            return RedirectToAction("Login");            
        }

        public ActionResult Reset(String txtEmail)
        {
            int num = emailUtility.SendEmail1(txtEmail);
            ViewBag.code = num;
            ViewBag.email = txtEmail;
            return View();
        }
        public ActionResult match(String txtCode)
        {
            String code = Request["code"];
            if (txtCode == code)
            {
                String email = Request["email"];
                var obj=PMS.BAL.UserBO.GetUserByEmail(email);
                return RedirectToAction("Signup", obj);
            }
            else
            {
                ViewBag.code = code;
                ViewBag.email = Request["email"];
                ViewBag.msg = "Wrong Code";
                return View("reset");
            }
        }

        [HttpGet]
        public ActionResult Logout()
        {
            SessionManager.ClearSession();
            return RedirectToAction("Login");
        }


        [HttpGet]
        public ActionResult Login2()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ValidateUser(String login, String password)
        {

            Object data = null;

            try
            {
                var url = "";
                var flag = false;

                var obj = PMS.BAL.UserBO.ValidateUser(login, password);
                if (obj != null)
                {
                    flag = true;
                    SessionManager.User = obj;

                    if (obj.IsAdmin == true)
                        url = Url.Content("~/Home/Admin");
                    else
                        url = Url.Content("~/Home/NormalUser");
                }

                data = new
                {
                    valid = flag,
                    urlToRedirect = url
                };
            }
            catch (Exception)
            {
                data = new
                {
                    valid = false,
                    urlToRedirect = ""
                };
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult showProfile()
        {
            int userId = Convert.ToInt32(Request.QueryString["userId"]);
            var obj = PMS.BAL.UserBO.GetUserById(userId);
            ViewBag.userProfile = obj;
            return View();
        }
	}
}